import numpy as np
from bitmanipulation import flip, get_bit

def construct_H(n, J=1, model="Heisenberg"):
    if model != "Heisenberg":
        raise RuntimeWarning("Model not implemented.")

    n_basis = 2**n # number of basis states
    H = np.zeros((n_basis, n_basis))
    for a in range(n_basis):
        for i in range(n-1):
            if get_bit(a, i) == get_bit(a, i+1):
                H[a, a] += .25
            else:
                H[a, a] -= .25
                b = flip(a, i)
                H[a, b] = .5

    return J*H
