import numpy as np
from numpy import linalg as LA

'''
#calculate coefficients from eigenvector matrix and initial state

def coefficients(psi_0, eigen_matrix):
    coeff = []
    for i in range(len(psi_0)):
        coeff.append( np.dot(np.conj(eigen_matrix[:,i]), psi_0)  )
    return coeff
'''

#calculate components in the eigenvector basis of the initial state

def psi(psi_0, eigen_matrix, eigen_val, t):
    coeff = np.dot(eigen_matrix.getH(),psi_0)
    exp_fact = np.exp(- eigen_val*t*1j)
   # psi_comp = coeff * exp_fact
    return (coeff, exp_fact)
    

psi_0 = np.array([1,1])

eigen_matrix = np.matrix([[1,  0],
                         [0,  2]])

eigen_val = np.array([1,2])

print(psi(psi_0, eigen_matrix, eigen_val, 1))




