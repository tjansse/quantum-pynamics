import matplotlib.pyplot as plt

def plot_observable_evolution(arr, tmax):
    "Plot the time evolution of an observable\
    \
    Input should be a 2d numpy.ndarray\
    Each row corresponds to the state at one time"

    n_spins = arr.shape[0]
    plt.matshow(arr, extent=(tmax, 0, 0, n_spins), aspect=1/5, cmap='PiYG')
    plt.colorbar()
    plt.show()

def plot_lines(t, arr):
    for i in range(arr.shape[1]):
        plt.plot(t, arr[:, i])

    plt.show()
