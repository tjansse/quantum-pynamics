import numpy as np
from numpy import linalg as LA

#calculate components in the eigenvector basis of the initial state
#given initial state (array of coefficients), eigenvector matrix and eigenvalues

def psi(psi_0, eigen_matrix, eigen_val, t):
    coeff = np.dot(np.conj(np.transpose(eigen_matrix)),psi_0)
    exp_fact = np.exp(- eigen_val*t*1j)
    psi_comp = coeff * exp_fact
    return psi_comp

#expectation value of an operator given its representation in the initial basis
#in the  state psi(t)

def observable(psi_0, operat_matrix, eigen_matrix, eigen_val, t):
    op_exp = LA.multi_dot((np.conj(psi(psi_0, eigen_matrix, eigen_val, t)),
                           np.conj(np.transpose(eigen_matrix)), operat_matrix,
                           eigen_matrix, psi(psi_0, eigen_matrix, eigen_val, t)))
    return op_exp

#print(observable(operat_matrix, eigen_matrix, 1))

