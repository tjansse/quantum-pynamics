import numpy as np
from bitmanipulation import get_bit

def S_z(n, i):
    n_basis = 2**n
    ret_arr = np.zeros((2**n, 2**n))
    for a in range(n_basis):
        if get_bit(a, i) == 1:
            ret_arr[a, a] = .5
        else:
            ret_arr[a, a] = -.5

    return ret_arr
