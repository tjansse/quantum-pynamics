import numpy as np
from hamiltonian import construct_H
from operat_exp import observable

def evolution(n, psi0, t, operator, J=1):
    ""
    # ToDo: check normalization and length of psi0

    H = construct_H(n, J)
    w, v = np.linalg.eigh(H)

    output = np.empty((t.shape[0], n))
    for i in range(n):
        operat_matrix = operator(n, i)
        for j, t_j in enumerate(t):
            output[j, i] = observable(psi0, operat_matrix, v, w, t_j)

    return output
