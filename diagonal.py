#This code will return eigenvalue and eigenvector for given input matrix
import numpy as np
from numpy import linalg as npla
#
def eigen(A):
    eigenValues, eigenVectors = npla.eig(A)
    idx = np.argsort(eigenValues)
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:,idx]
    return (eigenValues, eigenVectors)

D = np.random.random ((2,2))
a,b = eigen(D)

print a[0]
print b[0]

