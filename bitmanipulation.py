def flip(x, i):
    "flip the bits at positions i and i+1 in integer x"
    return x ^ (3 << i)

def get_bit(x, i):
    "get the bit at position i in integer x"
    return (x >> i) & 1
