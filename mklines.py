import numpy as np
from quapy import evolution
from plot import plot_lines
from operators import S_z

n = 9
psi0 = np.zeros(2**n)
psi0[2**4] = 1

J = .5
tmax = 200
t = np.linspace(0, tmax, 500)

output = evolution(n, psi0, t, S_z, J)
plot_lines(t, output)
